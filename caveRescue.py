 #Code Reference https://www.python-course.eu/graphs_python.php for graph object.

import random

class Graph(object):

    def __init__(self, graph_dict=None):
        """ initializes a graph object
            If no dictionary or None is given,
            an empty dictionary will be used
        """
        if graph_dict == None:
            graph_dict = {}
        self.__graph_dict = graph_dict

    def vertices(self):
        """ returns the vertices of a graph """
        return list(self.__graph_dict.keys())

    def edges(self):
        """ returns the edges of a graph """
        return self.__generate_edges()

    def add_vertex(self, vertex):
        """ If the vertex "vertex" is not in
            self.__graph_dict, a key "vertex" with an empty
            list as a value is added to the dictionary.
            Otherwise nothing has to be done.
        """
        if vertex not in self.__graph_dict:
            self.__graph_dict[vertex] = []

    def add_edge(self, edge):
        """ assumes that edge is of type set, tuple or list;
            between two vertices can be multiple edges!
        """
        edge = set(edge)
        (vertex1, vertex2) = tuple(edge)
        if vertex1 in self.__graph_dict:
            self.__graph_dict[vertex1].append(vertex2)
        else:
            self.__graph_dict[vertex1] = [vertex2]

    def __generate_edges(self):
        """ A static method generating the edges of the
            graph "graph". Edges are represented as sets
            with one (a loop back to the vertex) or two
            vertices
        """
        edges = []
        for vertex in self.__graph_dict:
            for neighbour in self.__graph_dict[vertex]:
                if {neighbour, vertex} not in edges:
                    edges.append({vertex, neighbour})
        return edges

    def get_path(self, vertex):
        """Returns the edges connected to a specified node"""
        paths=[]
        for neighbour in self.__graph_dict[vertex]:
            paths.append(neighbour)
            #if {neighbour, vertex} not in paths:
                #paths.append({vertex, neighbour})
        return paths

    def __str__(self):
        res = "vertices: "
        for k in self.__graph_dict:
            res += str(k) + " "
        res += "\nedges: "
        for edge in self.__generate_edges():
            res += str(edge) + " "
        return res

class RescueTeam(object):
    def teamnumb(self, teamNumber):
        self.teamNumber=teamNumber

    def greet(self):
        teamNumber=0
        greeting=("Rescue team " + str(self.teamNumber) + ", reporting!")
        return greeting

    def initLoc(self):
        positionGraph=graph.vertices()
        self.positionGraph=positionGraph[0]
        print("On position -> " + positionGraph[0])
        print("Can move to position ", graph.get_path(positionGraph[0]))
        print("")

    def moveTeam(self, rt):
        while True:
            print("Move rescue team", rescueTeam[rt].teamNumber, "to position: ", end='')#Shows rescue team being moved
            new=str(input(""))#input for new position
            if new not in graph.get_path(rescueTeam[rt].positionGraph):#if inputted position is not in the list of neighbouring verts
                print("Invalid input! Moves available: ", graph.get_path(rescueTeam[rt].positionGraph))#print message and restart question
            else:#if input is in graph
                break#exit loop
        rescueTeam[rt].positionGraph=new#rescue team has a new position
        print("Recue team", rescueTeam[rt].teamNumber, "now on position", rescueTeam[rt].positionGraph)#notify user of new position
        print("Can move to ", graph.get_path(rescueTeam[rt].positionGraph))#list next moves
        print("")
        return rescueTeam[rt].positionGraph#return value of new position

    def search(self, rt):
        print("Team:", rt) #Recsue team number
        localPos=rescueTeam[rt].positionGraph #Current position of recuse team on graph
        neighbours=graph.get_path(rescueTeam[rt].positionGraph) #Gets all neighbouring nodes of current position
        print("Can move to:", neighbours)#print out the list of neighbouring nodes
        print("On position", localPos)#printing current position
        for neighbour in neighbours:#for each in entry in the list of neighbouring nodes
            if set(neighbours) <= set(visited):#if the subset of where to visit, is in the set of total visited nodes
                for y in visited:#for each entry in the visited set
                    if y in neighbours:#if the entry from the subset is in the super set
                        visited.remove(y)#remove that element
                        break#break out of for loop
            if neighbour not in visited:#if entry in list of neighbouring nodes is not in the set of visited
                visited.append(neighbour)#add element to list
                print("Total visited:",visited)#print out list for debugging
                rescueTeam[rt].positionGraph=neighbour#move rescue team to new position
                print("Moved to", neighbour)#alert player
                print("")
                break#exit loop
        return rescueTeam[rt].positionGraph #return value of new position

class LostExplorer(object):

    def position(self):
        positionGraph=graph.vertices()
        positionGraph.remove('start')
        lostExplorerPosition=(random.choice(positionGraph))
        #print("Lost explorer on position -> " + lostExplorerPosition)

        return lostExplorerPosition

def createRescue():
    rt=int(input("Enter number of cave rescue teams: "))
    print("")
    global rescueTeam
    rescueTeam=[RescueTeam() for x in range(rt)]
    for x in range(rt):
        rescueTeam[x].teamNumber=x
        print(rescueTeam[x].greet())
        rescueTeam[x].initLoc()
    return rt

def gameSelect():
    print("")
    print("Decide whether you want the game to be played by yourself, or automated search")
    print("1) Automated")
    print("2) Player Control")
    print("0) Exit")
    gametype=(int(input("Enter number: ")))
    return gametype

def menu():
    print(" ")
    print("Enter size of graph")
    print("1) Small")
    print("2) Medium")
    print("3) Large")
    print("4) Custom")
    print("0) Exit")
    choice=(int(input("Enter number: ")))
    return choice

def run_graph():
    print("Vertices of graph:")
    print(graph.vertices())

    print("Edges of graph:")
    print(graph.edges())

def custom_graph():
    global graph#make the graph variable global for other functions to use
    graph = Graph(custom)#graph object takes value of custom graph
    run_graph()#show content of graph, currently one node 'start' and no edges
    while True:
        vertVar=str(input("Enter a single vertice to add to graph (type 'exit' to leave): "))#prompt user to input vert
        if vertVar=='exit':#if exit is chosen, move on to next loop
            break#exit loop
        if vertVar in graph.vertices():#if the vert is already a part of the graph
            print("Entry already exists!")#inform user of no duplications
            continue#restart loop
        graph.add_vertex(vertVar)#if no if statements are triggered, add the vert to the graph
        print(graph.vertices())#print list of verts as you go
    while True:
        edgeVar1=str(input("Enter starter node for edge connection (type 'exit' to leave): "))#prompt user to write the first node to connect the edge to
        if edgeVar1=='exit':#if value is exit, leave the loop
            break#exit loop
        if edgeVar1 not in graph.vertices():#if user input is not equal to a vert in the graph
            print("Option not in graph")#tell user input is wrong
            continue#restart loop
        edgeVar2=str(input("Enter secondary node to connect above node to: "))#prompt user to enter edge to connect graph to
        if edgeVar2 not in graph.vertices():#if user input is not equal to a vert in the graph
            print("Option not in graph")#tell user input is wrong
            continue#restart loop
        custom[edgeVar1].append(edgeVar2)#append custom graph variable for to connect the edge in a directed manner
    graph = Graph(custom)#graph is now equal to the value of custom, with all edges in place
    run_graph()#show the full graph, now it is completed


if __name__ == "__main__":
    loop=True
    global small, medium, large, custom


    small = { "start" : ["a", "b"],
          "a" : ["start", "c", "d"],
          "b" : ["start", "e", "f"],
          "c" : ["a"],
          "d" : ["a"],
          "e" : ["b"],
          "f" : ["b"]
        }

    medium = { "start" : ["a", "b", "start"],
          "a" : ["start", "c", "d"],
          "b" : ["start", "e", "f"],
          "c" : ["a"],
          "d" : ["a"],
          "e" : ["b"],
          "f" : ["b","g"],
          "g" : ["f","h"],
          "h" : ["g"],
        }

    large = { "start" : ["a", "b", "c", "d", "g"],
          "a" : ["start", "c", "d", "h"],
          "b" : ["start", "e", "f"],
          "c" : ["a"],
          "d" : ["a"],
          "e" : ["b"],
          "f" : ["b"],
          "g" : ["h", "i", "j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"],
          "h" : ["g", "a", "z"],
          "i" : ["g"],
          "j" : ["g"],
          "k" : ["g"],
          "l" : ["g"],
          "m" : ["g"],
          "n" : ["g"],
          "o" : ["g"],
          "p" : ["g"],
          "q" : ["g"],
          "r" : ["g"],
          "s" : ["g"],
          "t" : ["g"],
          "u" : ["g"],
          "v" : ["g"],
          "w" : ["g"],
          "x" : ["g"],
          "y" : ["g"],
          "z" : ["g", "h"],
        }

    custom = { "start" : []
        }

    while loop==True:
        visited = ["start",]
        gametype=gameSelect()
        choice=menu()
        if choice==0 or gametype==0:
            exit()
        if choice==1:
            graph = Graph(small)
        if choice==2:
            graph = Graph(medium)
        if choice==3:
            graph = Graph(large)
        if choice==4:
            custom_graph()
        if choice>4 or choice<0:
            print("Invalid input")
        else:
            lostExplorer=LostExplorer()
            lEP=lostExplorer.position()
            rt=createRescue()
            counter=0
            while True:
                counter+=1
                for x in range(rt):
                    if gametype==1:
                        run=rescueTeam[x].search(x)
                    if gametype==2:
                        run=rescueTeam[x].moveTeam(x)
                    if run == lEP:
                        print("")
                        print("FOUND THE LOST EXPLORER!")
                        print(counter, "turns")
                        print("")
                        exit()
            run_graph()
            print(graph.get_path("b"))
